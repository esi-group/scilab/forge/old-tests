getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/pol2tex_data.ref','rb');
s = poly(0, 's');
p = s^3 + 2 * s - 5;
%ans = pol2tex(p);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
