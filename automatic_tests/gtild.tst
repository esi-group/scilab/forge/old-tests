getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/gtild_data.ref','rb');
//Continuous time
s = poly(0, 's');G = [s,s^3;2 + s^3,s^2 - 5];
if load_ref('G') then   pause,end,

Gt = gtild(G, 'c');
if load_ref('Gt') then   pause,end,

%ans = Gt - horner(G, -s)';
if load_ref('%ans') then   pause,end,
//continuous-time interpretation
Gt = gtild(G, 'd');
%ans = Gt - horner(G, 1/s)' * (s^3);
if load_ref('%ans') then   pause,end,
//discrete-time interpretation
G = ssrand(2, 2, 3);Gt = gtild(G);//State-space (G is cont. time by default)
%ans = clean(horner(ss2tf(G), -s)' - ss2tf(Gt));
if load_ref('%ans') then   pause,end,
//Check
// Discrete-time
z = poly(0, 'z');
Gss = ssrand(2, 2, 3);Gss('dt') = 'd';//discrete-time
Gss(5) = [1,2;0,1];//With a constant D matrix
G = ss2tf(Gss);Gt1 = horner(G, 1/z)';
Gt = gtild(Gss);
Gt2 = clean(ss2tf(Gt));%ans = clean(Gt1 - Gt2);
if load_ref('%ans') then   pause,end,
//Check
//Improper systems
z = poly(0, 'z');
Gss = ssrand(2, 2, 3);Gss(7) = 'd';//discrete-time
Gss(5) = [z,z^2;1 + z,3];//D(z) is polynomial
G = ss2tf(Gss);Gt1 = horner(G, 1/z)';//Calculation in transfer form
Gt = gtild(Gss);//..in state-space
Gt2 = clean(ss2tf(Gt));%ans = clean(Gt1 - Gt2);
if load_ref('%ans') then   pause,end,
//Check
xdel_run(winsid());

mclose(%U);
