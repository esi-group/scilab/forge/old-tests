getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/hess_data.ref','rb');
A = rand(3, 3);[U,H] = hess(A);
%ans = and(abs(U * H * U' - A) < 0.0000000001);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
