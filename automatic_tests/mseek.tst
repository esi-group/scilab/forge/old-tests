getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/mseek_data.ref','rb');
file3 = 'test3.bin';
if load_ref('file3') then   pause,end,

fd1 = mopen(file3, 'wb');
for i = 1:10,  mput(i, 'd');end,
mseek(0);
mput(678, 'd');
mseek(0, fd1, 'end');
mput(932, 'd');
%ans = mclose(fd1);
if load_ref('%ans') then   pause,end,

fd1 = mopen(file3, 'rb');
res = mget(11, 'd');
if load_ref('res') then   pause,end,

res1 = 1:11;res1(1) = 678;res1($) = 932;
if res1 ~= res then   write(%io(2), 'Bug');end,
mseek(0, fd1, 'set');
// trying to read more than stored data
res1 = mget(100, 'd', fd1);
if res1 ~= res then   write(%io(2), 'Bug');end,
%ans = meof(fd1);
if load_ref('%ans') then   pause,end,

%ans = mclearerr(fd1);
if load_ref('%ans') then   pause,end,

mclose(fd1);
xdel_run(winsid());

mclose(%U);
