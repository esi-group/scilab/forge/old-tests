getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/dsimul_data.ref','rb');
z = poly(0, 'z');
h = (1 - 2 * z)/(z^2 - 0.2 * z + 1);
sl = tf2ss(h);
u = zeros(1, 20);u(1) = 1;
x1 = dsimul(sl, u);
if load_ref('x1') then   pause,end,
//Impulse response
u = ones(1, 20);
x2 = dsimul(sl, u);//Step response
xdel_run(winsid());

mclose(%U);
