getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/delip_data.ref','rb');
ck = 0.5;
%ans = delip([1,2], ck);
if load_ref('%ans') then   pause,end,

%ans = deff('y=f(t)', 'y=1/sqrt((1-t^2)*(1-ck^2*t^2))');
if load_ref('%ans') then   pause,end,

%ans = intg(0, 1, f);
if load_ref('%ans') then   pause,end,
//OK since real solution!
xdel_run(winsid());

mclose(%U);
