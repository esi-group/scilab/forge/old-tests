getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/justify_data.ref','rb');
t = ['1234','x','adfdfgdfghfgj';
  '1','354556','dgf';
  'sdfgd','','sdfsf'];

%ans = justify(t, 'l');
if load_ref('%ans') then   pause,end,

%ans = justify(t, 'c');
if load_ref('%ans') then   pause,end,


xdel_run(winsid());

mclose(%U);
