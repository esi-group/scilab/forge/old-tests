getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/sva_data.ref','rb');
A = rand(5, 4) * rand(4, 5);
[U,s,V] = sva(A, 2);
B = U * s * V';
%ans = svd(A);
if load_ref('%ans') then   pause,end,

%ans = svd(B);
if load_ref('%ans') then   pause,end,

%ans = clean(svd(A - B));
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
