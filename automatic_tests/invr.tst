getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/invr_data.ref','rb');
s = poly(0, 's');
if load_ref('s') then   pause,end,

H = [s,s * s + 2;1 - s,1 + s];%ans = invr(H);
if load_ref('%ans') then   pause,end,

[Num,den] = coffg(H);%ans = Num/den;
if load_ref('%ans') then   pause,end,

H = [1/s,s + 1;1/(s + 2),(s + 3)/s];%ans = invr(H);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
