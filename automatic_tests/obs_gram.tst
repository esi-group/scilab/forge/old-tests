getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/obs_gram_data.ref','rb');
A = -diag(1:3);C = rand(2, 3);
Go = obs_gram(A, C, 'c');// <=> w=syslin('c',A,[],C); Go=obs_gram(w);
%ans = norm(Go * A + A' * Go + C' * C, 1);
if load_ref('%ans') then   pause,end,

%ans = norm(lyap(A, -C' * C, 'c') - Go, 1);
if load_ref('%ans') then   pause,end,

A = A/4;Go = obs_gram(A, C, 'd');//discrete time case
%ans = norm(lyap(A, -C' * C, 'd') - Go, 1);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
