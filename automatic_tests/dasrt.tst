getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/dasrt_data.ref','rb');
//dy/dt = ((2*log(y)+8)/t -5)*y,  y(1) = 1,  1<=t<=6
//g1 = ((2*log(y)+8)/t - 5)*y
//g2 = log(y) - 2.2491
y0 = 1;t = 2:6;t0 = 1;y0d = 3;
atol = 0.000001;rtol = 0;ng = 2;

%ans = deff('[delta,ires]=res1(t,y,ydot)', 'ires=0;delta=ydot-((2*log(y)+8)/t-5)*y');
if load_ref('%ans') then   pause,end,

%ans = deff('[rts]=gr1(t,y)', 'rts=[((2*log(y)+8)/t-5)*y;log(y)-2.2491]');
if load_ref('%ans') then   pause,end,


[yy,nn] = dasrt([y0,y0d], t0, t, atol, rtol, res1, ng, gr1);
//(Should return nn=[2.4698972 2])
xdel_run(winsid());

mclose(%U);
