getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/st_deviation_data.ref','rb');
A = [1,2,10;7,7.1,7.01];
%ans = st_deviation(A);
if load_ref('%ans') then   pause,end,

%ans = st_deviation(A, 'r');
if load_ref('%ans') then   pause,end,

%ans = st_deviation(A, 'c');
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
