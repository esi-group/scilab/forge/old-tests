getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/sinm_data.ref','rb');
A = [1,2;2,4];
%ans = sinm(A) + 0.5 * %i * (expm(%i * A) - expm(-%i * A));
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
