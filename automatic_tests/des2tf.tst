getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/des2tf_data.ref','rb');
s = poly(0, 's');
G = [1/(s + 1),s;1 + s^2,3 * (s^3)];
Descrip = tf2des(G);Tf1 = des2tf(Descrip);
if load_ref('Tf1') then   pause,end,

Descrip2 = tf2des(G, 'withD');Tf2 = des2tf(Descrip2);
if load_ref('Tf2') then   pause,end,

[A,B,C,D,E] = Descrip2(2:6);Tf3 = C * inv(s * E - A) * B + D;
if load_ref('Tf3') then   pause,end,

xdel_run(winsid());

mclose(%U);
