getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/inv_data.ref','rb');
A = rand(3, 3);%ans = inv(A) * A;
if load_ref('%ans') then   pause,end,

//
x = poly(0, 'x');
A = [x,1,x;x^2,2,1 + x;1,2,3];%ans = inv(A) * A;
if load_ref('%ans') then   pause,end,

//
A = [1/x,2;2 + x,2/(1 + x)];
if load_ref('A') then   pause,end,

%ans = inv(A) * A;
if load_ref('%ans') then   pause,end,

//
A = ssrand(2, 2, 3);
W = inv(A) * A;
if load_ref('W') then   pause,end,

%ans = clean(ss2tf(W));
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
