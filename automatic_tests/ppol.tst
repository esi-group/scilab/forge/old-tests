getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/ppol_data.ref','rb');
A = rand(3, 3);B = rand(3, 2);
F = ppol(A, B, [-1,-2,-3]);
%ans = spec(A - B * F);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
