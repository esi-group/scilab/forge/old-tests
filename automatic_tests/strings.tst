getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/strings_data.ref','rb');
%ans = ['this','is';'a 2x2','matrix'];
if load_ref('%ans') then   pause,end,

%ans = 'matrix' == ('mat' + 'rix');
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
