getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/mlist_data.ref','rb');

M = mlist(['V','name','value'], ['a','b','c'], [1,2,3]);
//define display
%ans = deff('%V_p(M)', 'disp(M.name+'':''+string(M.value))');
if load_ref('%ans') then   pause,end,

//define extraction operation
%ans = deff('r=%V_e(i,M)', 'r=mlist([''V'',''name'',''value''],M.name(i),M.value(i))');
if load_ref('%ans') then   pause,end,

%ans = M(2);
if load_ref('%ans') then   pause,end,
// the second entry of the vector M
%ans = M('value');
if load_ref('%ans') then   pause,end,


//define M as a tlist
M = tlist(['V','name','value'], ['a','b','c'], [1,2,3]);
%ans = M(2);
if load_ref('%ans') then   pause,end,


%ans = M('name');
if load_ref('%ans') then   pause,end,


//with two  indices
M = mlist(['V','name','value'], ['a','b';'c','d'], [1,2;3,4]);
%ans = deff('r=%V_e(varargin)', [
  'M=varargin($)';
  'H=[''V'',''name'',''value'']';
  'r=mlist(H,M.name(varargin(1:$-1)),M.value(varargin(1:$-1)))']);
if load_ref('%ans') then   pause,end,


%ans = M(:, 2);
if load_ref('%ans') then   pause,end,

// multidimensionnal array
str = ['a','b','c','d','e','f','g','h'];
n = hypermat([2,2,2], str);
v = hypermat([2,2,2], 1:8);
M = mlist(['V','name','value'], n, v);
%ans = M(1, 1:2, 2);
if load_ref('%ans') then   pause,end,


xdel_run(winsid());

mclose(%U);
