getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/kroneck_data.ref','rb');
F = randpencil([1,1,2], [2,3], [-1,3,1], [0,3]);
Q = rand(17, 17);Z = rand(18, 18);F = Q * F * Z;
//random pencil with eps1=1,eps2=1,eps3=1; 2 J-blocks @ infty
//with dimensions 2 and 3
//3 finite eigenvalues at -1,3,1 and eta1=0,eta2=3
[Q,Z,Qd,Zd,numbeps,numbeta] = kroneck(F);
%ans = [Qd(1),Zd(1)];
if load_ref('%ans') then   pause,end,
//eps. part is sum(epsi) x (sum(epsi) + number of epsi)
%ans = [Qd(2),Zd(2)];
if load_ref('%ans') then   pause,end,
//infinity part
%ans = [Qd(3),Zd(3)];
if load_ref('%ans') then   pause,end,
//finite part
%ans = [Qd(4),Zd(4)];
if load_ref('%ans') then   pause,end,
//eta part is (sum(etai) + number(eta1)) x sum(etai)
numbeps;
if load_ref('numbeps') then   pause,end,

numbeta;
if load_ref('numbeta') then   pause,end,

xdel_run(winsid());

mclose(%U);
