getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/ode_root_data.ref','rb');
// Integration of the differential equation
// dy/dt=y , y(0)=1, and finds the minimum time t such that y(t)=2
%ans = deff('[ydot]=f(t,y)', 'ydot=y');
if load_ref('%ans') then   pause,end,

%ans = deff('[z]=g(t,y)', 'z=y-2');
if load_ref('%ans') then   pause,end,

y0 = 1;ng = 1;
[y,rd] = ode('roots', y0, 0, 2, f, ng, g);
if load_ref('rd') then   pause,end,
if load_ref('y') then   pause,end,



%ans = deff('[z]=g(t,y)', 'z=y-[2;2;33]');
if load_ref('%ans') then   pause,end,

[y,rd] = ode('roots', 1, 0, 2, f, 3, g);
if load_ref('rd') then   pause,end,
if load_ref('y') then   pause,end,



xdel_run(winsid());

mclose(%U);
