getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/setfield_data.ref','rb');
l = list(1, 'qwerw', %s);
if load_ref('l') then   pause,end,

l(1) = 'Changed';
if load_ref('l') then   pause,end,

l(0) = 'Added';
if load_ref('l') then   pause,end,

l(6) = ['one more';'added'];
if load_ref('l') then   pause,end,

//

a = hypermat([2,2,2], rand(1:2^3));// hypermatrices are coded using mlists
setfield(3, 1:8, a);a;
if load_ref('a') then   pause,end,
/// set the field value to 1:8
xdel_run(winsid());

mclose(%U);
