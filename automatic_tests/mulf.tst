getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/mulf_data.ref','rb');
%ans = mulf('1', 'a');
if load_ref('%ans') then   pause,end,

%ans = mulf('0', 'a');
if load_ref('%ans') then   pause,end,

%ans = 'a' + 'b';
if load_ref('%ans') then   pause,end,
//Caution...
xdel_run(winsid());

mclose(%U);
