getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/hypermat_data.ref','rb');
M = hypermat([2,3,2,2], 1:24);
if load_ref('M') then   pause,end,

xdel_run(winsid());

mclose(%U);
