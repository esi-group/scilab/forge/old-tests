getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/rmdir_data.ref','rb');
%ans = mkdir(SCI, 'Directory');
if load_ref('%ans') then   pause,end,

%ans = rmdir(SCI + '/Directory');
if load_ref('%ans') then   pause,end,


xdel_run(winsid());

mclose(%U);
