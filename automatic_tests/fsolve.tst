getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/fsolve_data.ref','rb');
// A simple example with fsolve
a = [1,7;2,8];b = [10;11];
deff('[y]=fsol1(x)', 'y=a*x+b');
deff('[y]=fsolj1(x)', 'y=a');
xres = fsolve([100;100], fsol1);
%ans = a * xres + b;
if load_ref('%ans') then   pause,end,

xres = fsolve([100;100], fsol1, fsolj1);
%ans = a * xres + b;
if load_ref('%ans') then   pause,end,

// See routines/default/Ex-fsolve.f
xres = fsolve([100;100], 'fsol1', 'fsolj1', 0.0000001);
%ans = a * xres + b;
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
