getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/st_ility_data.ref','rb');
A = diag([0.9,-2,3]);B = [0;0;1];Sl = syslin('c', A, B, []);
[ns,nc,U] = st_ility(Sl);
%ans = U' * A * U;
if load_ref('%ans') then   pause,end,

%ans = U' * B;
if load_ref('%ans') then   pause,end,

[ns,nc,U] = st_ility(syslin('d', A, B, []));
%ans = U' * A * U;
if load_ref('%ans') then   pause,end,

%ans = U' * B;
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
