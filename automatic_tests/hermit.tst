getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/hermit_data.ref','rb');
s = poly(0, 's');
p = [s,s * ((s + 1)^2),2 * (s^2) + s^3];
[Ar,U] = hermit(p' * p);
%ans = clean(p' * p * U);
if load_ref('%ans') then   pause,end,
%ans = det(U);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
