getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/bezout_data.ref','rb');
// polynomial case
x = poly(0, 'x');
p1 = (x + 1) * ((x - 3)^5);p2 = (x - 2) * ((x - 3)^3);
[thegcd,U] = bezout(p1, p2);
if load_ref('U') then   pause,end,
if load_ref('thegcd') then   pause,end,

%ans = det(U);
if load_ref('%ans') then   pause,end,

%ans = clean([p1,p2] * U);
if load_ref('%ans') then   pause,end,

thelcm = p1 * U(1, 2);
if load_ref('thelcm') then   pause,end,

%ans = lcm([p1,p2]);
if load_ref('%ans') then   pause,end,

// integer case
i1 = int32(2 * (3^5));i2 = int32((2^3) * (3^2));
[thegcd,U] = bezout(i1, i2);
if load_ref('U') then   pause,end,
if load_ref('thegcd') then   pause,end,

V = int32([(2^2) * (3^5),(2^3) * (3^2),(2^2) * (3^4) * 5]);
[thegcd,U] = gcd(V);
if load_ref('U') then   pause,end,
if load_ref('thegcd') then   pause,end,

%ans = V * U;
if load_ref('%ans') then   pause,end,

%ans = lcm(V);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
