getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/automatic_tests/spget_data.ref','rb');
sp = sparse([1,2;4,5;3,10], [1,2,3]);
if load_ref('sp') then   pause,end,

[ij,v,mn] = spget(sp);
xdel_run(winsid());

mclose(%U);
