getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/xarrows_data.ref','rb');
x = 2 * %pi * (0:9)/8;
x1 = [sin(x);9 * sin(x)];
y1 = [cos(x);9 * cos(x)];
%ans = plot2d([-10,10], [-10,10], [-1,-1], '022');
if load_ref('%ans') then   pause,end,

%ans = xset('clipgrf');
if load_ref('%ans') then   pause,end,

%ans = xarrows(x1, y1, 1, 1:10);
if load_ref('%ans') then   pause,end,

%ans = xset('clipoff');
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
