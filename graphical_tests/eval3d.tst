getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/eval3d_data.ref','rb');
x = -5:5;y = x;
deff('[z]=f(x,y)', 'z= x.*y');
z = eval3d(f, x, y);
plot3d(x, y, z);
//
deff('[z]=f(x,y)', 'z= x*y');
z = feval(x, y, f);
plot3d(x, y, z);
xdel_run(winsid());

mclose(%U);
