getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/interp1_data.ref','rb');
x = linspace(0, 3, 20);
y = x^2;
xx = linspace(0, 3, 100);
yy1 = interp1(x, y, xx, 'linear');
yy2 = interp1(x, y, xx, 'spline');
yy3 = interp1(x, y, xx, 'nearest');
%ans = plot(xx, [yy1;yy2;yy3], x, y, '*');
if load_ref('%ans') then   pause,end,

%ans = xtitle('interpolation of square function');
if load_ref('%ans') then   pause,end,

%ans = legend(['linear','spline','nearest'], a=2);
if load_ref('%ans') then   pause,end,


xdel_run(winsid());

mclose(%U);
