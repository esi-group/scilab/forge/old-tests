getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/contour2di_data.ref','rb');
[xc,yc] = contour2di(1:10, 1:10, rand(10, 10), 5);
k = 1;n = yc(k);c = 1;
while (k + yc(k)) < size(xc, '*') then
  n = yc(k);
  %ans = plot2d(xc(k + (1:n)), yc(k + (1:n)), c);
  if load_ref('%ans') then   pause,end,

  c = c + 1;
  k = k + n + 1;
end,

xdel_run(winsid());

mclose(%U);
