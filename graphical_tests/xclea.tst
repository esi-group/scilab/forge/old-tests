getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/xclea_data.ref','rb');
x = (0:0.1:2 * %pi)';
%ans = plot2d(x, sin(x));
if load_ref('%ans') then   pause,end,

%ans = xclea(1, 1, 1, 1);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
