getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/hsvcolormap_data.ref','rb');
t = (0:0.1:2 * %pi)';z = sin(t) * cos(t');
f = gcf();f('color_map') = hsvcolormap(64);
%ans = plot3d1(t, t, z, 35, 45, 'X@Y@Z', [-2,2,2]);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
