getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/xstringl_data.ref','rb');

%ans = plot2d([0;1], [0;1], 0);
if load_ref('%ans') then   pause,end,

str = ['Scilab','is';'not','elisaB'];
r = xstringl(0.5, 0.5, str);
if load_ref('r') then   pause,end,

%ans = xrects([r(1),r(2) + r(4),r(3),r(4)]');
if load_ref('%ans') then   pause,end,

%ans = xstring(r(1), r(2), str);
if load_ref('%ans') then   pause,end,



%ans = plot2d([0;1], [0;1], 0);
if load_ref('%ans') then   pause,end,

str = ['Scilab','n''est ';'pas','Matlab'];
r2 = xstringl(0.5, 0.5, str, 2, 5);
if load_ref('r2') then   pause,end,

%ans = xrects([r2(1),r2(2) + r2(4),r2(3),r2(4)]');
if load_ref('%ans') then   pause,end,

%ans = xstring(r2(1), r2(2), str);
if load_ref('%ans') then   pause,end,


e = gce();
e.children(1).font_size = 5;
e.children(1).font_style = 2;
e.children(2).font_size = 5;
e.children(2).font_style = 2;
xdel_run(winsid());

mclose(%U);
