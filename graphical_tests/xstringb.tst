getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/xstringb_data.ref','rb');
str = ['Scilab','is';'not','elisaB'];
plot2d(0, 0, [-1,1], '010', ' ', [0,0,1,1]);
r = [0,0,1,0.5];
xstringb(r(1), r(2), str, r(3), r(4), 'fill');
%ans = xrect(r(1), r(2) + r(4), r(3), r(4));
if load_ref('%ans') then   pause,end,

r = [r(1),r(2) + r(4) + 0.01,r(3),r(4)/2];
%ans = xrect(r(1), r(2) + r(4), r(3), r(4));
if load_ref('%ans') then   pause,end,

xstringb(r(1), r(2), str, r(3), r(4), 'fill');
r = [r(1),r(2) + r(4) + 0.01,r(3),r(4)/2];
%ans = xrect(r(1), r(2) + r(4), r(3), r(4));
if load_ref('%ans') then   pause,end,

xstringb(r(1), r(2), str, r(3), r(4), 'fill');
xdel_run(winsid());

mclose(%U);
