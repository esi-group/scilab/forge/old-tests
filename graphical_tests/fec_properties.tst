getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/fec_properties_data.ref','rb');


set('figure_style', 'new');//create a figure

x = -10:10;y = -10:10;m = rand(21, 21);
Sgrayplot(x, y, m);
a = get('current_axes');
f = a.children.children(1);
if load_ref('f') then   pause,end,

f.data(eye(), 3) = (1:size(f('data'), 1))';
a.parent.color_map = hotcolormap(64);


xdel_run(winsid());

mclose(%U);
