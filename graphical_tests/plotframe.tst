getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/plotframe_data.ref','rb');
x = (-0.3:0.8:27.3)';
y = rand(x);
rect = [min(x),min(y),max(x),max(y)];
tics = [4,10,2,5];//4 x-intervals and 2 y-intervals
%ans = plotframe(rect, tics, [%f,%f], ['My plot','x','y'], [0,0,0.5,0.5]);
if load_ref('%ans') then   pause,end,

%ans = plot2d(x, y, 2, '000');
if load_ref('%ans') then   pause,end,

%ans = plotframe(rect, tics, [%t,%f], ['My plot with grids','x','y'], [0.5,0,0.5,0.5]);
if load_ref('%ans') then   pause,end,

%ans = plot2d(x, y, 3, '000');
if load_ref('%ans') then   pause,end,

%ans = plotframe(rect, tics, [%t,%t], ['My plot with grids and automatic bounds','x','y'], [0,0.5,0.5,0.5]);
if load_ref('%ans') then   pause,end,

%ans = plot2d(x, y, 4, '000');
if load_ref('%ans') then   pause,end,

%ans = plotframe(rect, tics, [%f,%t], ['My plot without grids but with automatic bounds','x','y'], [0.5,0.5,0.5,0.5]);
if load_ref('%ans') then   pause,end,

%ans = plot2d(x, y, 5, '000');
if load_ref('%ans') then   pause,end,

%ans = xset('default');
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
