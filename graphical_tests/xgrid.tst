getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/xgrid_data.ref','rb');
x = (0:0.1:2 * %pi)';
%ans = plot2d(sin(x));
if load_ref('%ans') then   pause,end,

%ans = xgrid(2);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
