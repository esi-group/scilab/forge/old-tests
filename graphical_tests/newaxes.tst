getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/newaxes_data.ref','rb');
%ans = clf_run();
if load_ref('%ans') then   pause,end,

a1 = newaxes();
a1('axes_bounds') = [0,0,1,0.5];
t = 0:0.1:20;
%ans = plot(t, acosh(t), 'r');
if load_ref('%ans') then   pause,end,

a2 = newaxes();
a2('axes_bounds') = [0,0.5,1,0.5];
x = 0:0.1:4;
%ans = plot(x, sinh(x));
if load_ref('%ans') then   pause,end,

%ans = legend('sinh');
if load_ref('%ans') then   pause,end,


sca(a1);//make first axes current
%ans = plot(t, asinh(t), 'g');
if load_ref('%ans') then   pause,end,

%ans = legend(['acosh','asinh']);
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
