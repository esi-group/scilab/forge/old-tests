getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/graypolarplot_data.ref','rb');


rho = 1:0.1:4;theta = (0:0.02:1) * 2 * %pi;
z = 30 + round(theta' * (1 + rho^2));
f = gcf();
f('color_map') = hotcolormap(128);
clf_run();%ans = graypolarplot(theta, rho, z);
if load_ref('%ans') then   pause,end,


xdel_run(winsid());

mclose(%U);
