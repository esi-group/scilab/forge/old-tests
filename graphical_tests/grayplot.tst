getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/grayplot_data.ref','rb');

x = -10:10;y = -10:10;m = rand(21, 21);
%ans = grayplot(x, y, m, rect=[-20,-20,20,20]);
if load_ref('%ans') then   pause,end,

t = -%pi:0.1:%pi;m = sin(t)' * cos(t);
%ans = clf_run();
if load_ref('%ans') then   pause,end,

%ans = grayplot(t, t, m);
if load_ref('%ans') then   pause,end,


xdel_run(winsid());

mclose(%U);
