getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/dsearch_data.ref','rb');
// example #1 (elementary stat for U(0,1))
m = 50000;n = 10;
X = grand(m, 1, 'def');
val = linspace(0, 1, n + 1)';
[ind,occ] = dsearch(X, val);
xbasc_run();%ans = plot2d2(val, [occ/m;0]);
if load_ref('%ans') then   pause,end,
// no normalisation : y must be near 1/n


// example #2 (elementary stat for B(N,p))
N = 8;p = 0.5;m = 50000;
X = grand(m, 1, 'bin', N, p);val = (0:N)';
[ind,occ] = dsearch(X, val, 'd');
Pexp = occ/m;Pexa = binomial(p, N);
xbasc_run();hm = 1.1 * max(max(Pexa), max(Pexp));
%ans = plot2d3([val,val + 0.1], [Pexa',Pexp], [1,2], '111', 'Pexact@Pexp', [-1,0,N + 1,hm], [0,N + 2,0,6]);
if load_ref('%ans') then   pause,end,

%ans = xtitle('binomial distribution B(' + string(N) + ',' + string(p) + ') :' + ' exact probability versus experimental ones');
if load_ref('%ans') then   pause,end,



// example #3 (piecewise Hermite polynomial)
x = [0;0.2;0.35;0.5;0.65;0.8;1];
y = [0;0.1;-0.1;0;0.4;-0.1;0];
d = [1;0;0;1;0;0;-1];
X = linspace(0, 1, 200)';
ind = dsearch(X, x);
// define Hermite base functions
%ans = deff('y=Ll(t,k,x)', 'y=(t-x(k+1))./(x(k)-x(k+1))');
if load_ref('%ans') then   pause,end,
// Lagrange left on Ik
%ans = deff('y=Lr(t,k,x)', 'y=(t-x(k))./(x(k+1)-x(k))');
if load_ref('%ans') then   pause,end,
// Lagrange right on Ik
%ans = deff('y=Hl(t,k,x)', 'y=(1-2*(t-x(k))./(x(k)-x(k+1))).*Ll(t,k,x).^2');
if load_ref('%ans') then   pause,end,

%ans = deff('y=Hr(t,k,x)', 'y=(1-2*(t-x(k+1))./(x(k+1)-x(k))).*Lr(t,k,x).^2');
if load_ref('%ans') then   pause,end,

%ans = deff('y=Kl(t,k,x)', 'y=(t-x(k)).*Ll(t,k,x).^2');
if load_ref('%ans') then   pause,end,

%ans = deff('y=Kr(t,k,x)', 'y=(t-x(k+1)).*Lr(t,k,x).^2');
if load_ref('%ans') then   pause,end,

// plot the curve
Y = y(ind) .* Hl(X, ind) + y(ind + 1) .* Hr(X, ind) + d(ind) .* Kl(X, ind) + d(ind + 1) .* Kr(X, ind);
xbasc_run();plot2d(X, Y, 2);%ans = plot2d(x, y, -9, '000');
if load_ref('%ans') then   pause,end,

%ans = xtitle('an Hermite piecewise polynomial');
if load_ref('%ans') then   pause,end,

// NOTE : you can verify by adding these ones :
// YY = interp(X,x,y,d); plot2d(X,YY,3,"000")
xdel_run(winsid());

mclose(%U);
