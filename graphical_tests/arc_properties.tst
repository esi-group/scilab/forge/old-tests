getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/arc_properties_data.ref','rb');
%ans = set('figure_style', 'new');
if load_ref('%ans') then   pause,end,
//create a figure
a = get('current_axes');//get the handle of the newly created axes
a('data_bounds') = [-2,-2;2,2];

%ans = xarc(-1.5, 1.5, 3, 3, 0, 360 * 64);
if load_ref('%ans') then   pause,end,


arc = get('hdl');//get handle on current entity (here the arc entity)
arc('fill_mode') = 'on';
arc('foreground') = 5;
arc.data(eye(), [3,6]) = [2,270 * 64];
xfarc(-0.5, 1, 0.4, 0.6, 0, 360 * 64);
arc('visible') = 'off';

xdel_run(winsid());

mclose(%U);
