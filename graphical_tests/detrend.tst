getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/detrend_data.ref','rb');
// example #1
t = linspace(0, 16 * %pi, 1000)';
x = -20 + t + 0.3 * sin(0.5 * t) + sin(t) + 2 * sin(2 * t) + 0.5 * sin(3 * t);
y = detrend(x);
%ans = xbasc_run();
if load_ref('%ans') then   pause,end,

%ans = plot2d(t, [x,y], style=[2,5]);
if load_ref('%ans') then   pause,end,

legend(['before detrend','after detrend']);
%ans = xgrid();
if load_ref('%ans') then   pause,end,


// example #2
t = linspace(0, 32 * %pi, 2000)';
x = abs(t - 16 * %pi) + 0.3 * sin(0.5 * t) + sin(t) + 2 * sin(2 * t) + 0.5 * sin(3 * t);
y = detrend(x, 'linear', 1000);
%ans = xbasc_run();
if load_ref('%ans') then   pause,end,

%ans = plot2d(t, [x,y], style=[2,5]);
if load_ref('%ans') then   pause,end,

legend(['before detrend','after detrend']);
%ans = xgrid();
if load_ref('%ans') then   pause,end,

xdel_run(winsid());

mclose(%U);
