getf SCI/util/testexamples.sci
reinit_for_test()
%U=mopen('SCI/tests/graphical_tests/legend_data.ref','rb');
%ans = set('figure_style', 'new');
if load_ref('%ans') then   pause,end,

t = 0:0.1:2 * %pi;
a = gca();a('data_bounds') = [t(1),-1.8;t($),1.8];

plot2d(t, [cos(t'),cos(2 * t'),cos(3 * t')], [-1,2,3]);
e = gce();
e.children(1).thickness = 3;
e.children(2).line_style = 4;

hl = legend(['cos(t)';'cos(2*t)';'cos(3*t)'], a=1);
xdel_run(winsid());

mclose(%U);
